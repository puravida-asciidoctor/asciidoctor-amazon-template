= Asciidoctor Template: Un texto, 3 formatos
Jorge Aguilera
:idprefix:
:idseparator: -
:imagesdir: images
ifeval::["{backend}" == "html5"]
:toc: left
endif::[]
//
// Configuracion no epub
ifndef::ebook-format[]
:leveloffset: 1
endif::ebook-format[]
//
// Configuracion epub
ifdef::ebook-format[]
:front-cover-image: image:jacket/cover.png[Front Cover,1050,1600]
:doctype: book
:publication-type: book
:producer: Asciidoctor
:keywords: Asciidoctor, samples, e-book, EPUB3, KF8, MOBI
:copyright: CC-BY-SA 3.0
// Si quieres que NO aparezca el avatar y el autor usa :publication-type: book
// Si quieres que aparezca usa: anthology, magazine, journal o article
:publication-type: anthology
:username: jorge-aguilera
endif::ebook-format[]

ifeval::["{backend}" == "html5"]
TIP: Leelo en Pdf link:index.pdf[]

TIP: Leelo en Amazon (mobi) link:index.mobi[]
endif::[]

include::chapters/creativecommons.adoc[]
include::chapters/readme.adoc[]
include::chapters/chapter1.adoc[]
include::chapters/chapter2.adoc[]
include::chapters/chapter3.adoc[]
include::chapters/chapter4.adoc[]
include::chapters/chaptern.adoc[]
