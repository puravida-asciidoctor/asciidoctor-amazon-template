= Introducción

Este es un tutorial para crear y publicar un libro en tres formatos diferentes (html, pdf y kindle de Amazon)
usando Asciidoctor como herramienta de generación.

De forma resumida, Asciidoc es una sintáxis basada en ficheros
de texto plano, lo que significa que puedes usar el editor más básico que
encuentres, con una lista de funcionalidades muy amplia que te permitirá definir elementos
como parráfos, capítulos, tablas, imágenes y un largo etc.

Asciidoctor por su parte es una implementación de este lenguaje que
es capaz de leer tus ficheros y generar diferentes salidas a partir
de ellos. Así por ejemplo con el mismo fichero que contiene tu texto 
podrás obtener una versión HTML, otra Pdf e incluso EPUB que es el
formato que te pedirá Amazón si quieres publicar un libro en su plataforma

Para conocer más sobre la sintáxis de Asciidoc consulta la
página web https://asciidoctor.org/docs/user-manual


Una vez que conozcas los elementos básicos de esta sintáxis serás capaz
de saber qué ficheros de esta plantilla tienes que ir modificando para
construir tu libro.

Por otra parte, para facilitar la descarga e instalación de herramientas
y demás procesos tediosos, esta plantilla requiere únicamente una 
versión de Java 8 (como mínimo) instalada en tu máquina. Así mismo es
bueno usar algún tipo de control de versiones tipo Git, pero NO ES
NECESARIO para este tutorial.


WARNING: Aunque he intentado tener en cuenta el sistema Windows en las
instructiones, este tutorial ha sido probado en un sistema Linux por lo que
puede que algunos comandos no se comporte exactamente como se indica en el mismo




