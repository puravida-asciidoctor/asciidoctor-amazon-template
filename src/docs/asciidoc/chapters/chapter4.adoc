= Amazon Kdp

ifeval::["{backend}" != "html5"]
Todo el código de este proyecto se encuentra alojado de forma
pública en https://gitlab.com/puravida-software/asciidoctor-amazon-template
endif::[]


Para publicar en Amazon tienes que tener una cuenta en https://kdp.amazon.com/

Una vez abierta e identificado accedes a la consola que te permite mantener tu
biblioteca de publicaciones.

Existen infinito número de tutoriales y vídeos de cómo usar esta herramienta así
que te remito a ellos.

Cuando llegues a la parte de subir tu manuscrito deberás adjuntar la última versión
que se encuentre en `build/docs/asciidocEpub/index.mobi`

WARNING: En dicho directorio existe un fichero `build/asciidoc/epub3/index-kf8.epub`
QUE NO ES EL QUE HAY QUE ADJUNTAR. El nombre puede producir a engaño pero este
fichero es simplemente un fichero de trabajo.

